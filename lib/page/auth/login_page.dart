import 'package:flutter/material.dart';

import 'package:dsarrolloapp/widgets/widgets.dart';

//import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.90,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Logo(
                  title: Text(
                    'Iniciar Sesión',
                    style: TextStyle(fontSize: 30),
                  ),
                  //TODO: agregar ruta logo
                  image: 'assets/img/logo.png',
                ),
                _Form(),
                SizedBox(height: 20),
                Labels(
                  text: '¿Olvidó su contraseña?',
                  onTap: () {
                    Navigator.pushNamed(context, 'recover');
                  },
                ),
                SizedBox(height: 20),
                Labels(
                  text: 'Abrir nueva cuenta ',
                  onTap: () {},
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Form extends StatefulWidget {
  _Form({Key key}) : super(key: key);

  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final emailController = TextEditingController();
  final passController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    //final authService = Provider.of<AuthService>(context);
    return Container(
      margin: EdgeInsets.only(top: 40),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: [
          CustomInput(
            icon: Icons.mail_outline,
            placeholder: 'Dirección de correo electronico',
            keyboardType: TextInputType.emailAddress,
            textController: emailController,
            //onChanged: _validateInput,
          ),
          CustomInput(
            icon: Icons.lock_outline,
            placeholder: 'Clave o contraseña',
            textController: passController,
            isPassword: true,
            //onChanged: _validateInput,
          ),
          ButtonBlue(
              text: 'Ingrese',
              onPressed:
                  () {} /*authService.autenticando
                ? null
                : () async {
                    FocusScope.of(context).unfocus();
                    Map<String, dynamic> data = {
                      'app': Environment.typeApp,
                      'app_version': Environment.version,
                      'id_device': await AuthService.getIdDevice(),
                      'device': await AuthService.getNameDivice(),
                      'user': emailController.text.trim(),
                      'password': passController.text.trim(),
                      'lang': 'es_VE',
                    };
                    final loginOk = await authService.login(data);
                    if (loginOk['ok']) {
                      Navigator.pushReplacementNamed(context, 'home');
                    } else {
                      // Mostrar alerta
                      customAlert(
                        context,
                        'Login incorrecto',
                        loginOk['msg'],
                      );
                    }
                  },*/
              ),
        ],
      ),
    );
  }

  /* _validateInput(_) {
    if (emailController.text != '' && passController.text != '') {
      Provider.of<AuthService>(context, listen: false).autenticando = false;
    } else {
      Provider.of<AuthService>(context, listen: false).autenticando = true;
    }
  }*/
}
