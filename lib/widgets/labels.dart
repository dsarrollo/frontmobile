part of 'widgets.dart';

class Labels extends StatelessWidget {
  final String text;
  final Function onTap;

  const Labels({
    Key key,
    this.text,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: this.onTap,
        child: Text(
          this.text,
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 18,
          ),
        ),
      ),
    );
  }
}
