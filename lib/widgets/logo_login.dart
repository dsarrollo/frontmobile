part of 'widgets.dart';

class Logo extends StatelessWidget {
  final Widget title;
  final String image;

  const Logo({
    Key key,
    @required this.title,
    @required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 250,
        margin: EdgeInsets.only(top: 50),
        child: Column(
          children: [
            //TODO: Quitar comentario al agregar logo
            /*Image(
              image: AssetImage(this.image),
            ),
            SizedBox(
              height: 20,
            ),*/
            title
          ],
        ),
      ),
    );
  }
}
